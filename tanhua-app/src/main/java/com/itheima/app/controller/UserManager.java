package com.itheima.app.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.itheima.autoconfig.sms.SmsTemplate;
import com.itheima.domain.db.User;
import com.itheima.domain.vo.ErrorResult;
import com.itheima.service.db.UserService;
import com.itheima.util.ConstantUtil;
import com.itheima.util.JwtUtil;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Component
public class UserManager {

    @DubboReference
    private UserService userService;

    @Autowired
    private SmsTemplate smsTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //1.保存用户
    public ResponseEntity save(User user){
        Long userId = userService.save(user);
        return ResponseEntity.ok(userId);
    }


    //2.更具手机号查询
    public ResponseEntity findByPhone(String phone){
        try{

        }catch (Exception e){
            e.printStackTrace();
            //自定义状态和响应体
            return ResponseEntity.status(500).body(ErrorResult.error());
        }

        User user = userService.findByPhone(phone);
        //状态码固定200
        return ResponseEntity.ok(user);
    }

    // 发送短信验证码
    public void sendSms(String phone) {
        // 1.生成6位随机数
        String code = RandomUtil.randomNumbers(6);
        code = "123456";
        // 2.调用阿里云发送短信
        // smsTemplate.sendSms(phone, code);  // TODO 产品上线后实现短信发送

        // 3.将验证码存入redis
        stringRedisTemplate.opsForValue().set(ConstantUtil.SMS_CODE +phone, code, Duration.ofMinutes(5));
    }

    // 注册登录
    public ResponseEntity regAndLogin(String phone, String verificationCode) {
        // 1.取出redis中验证码
        String codeFromRedis = stringRedisTemplate.opsForValue().get(ConstantUtil.SMS_CODE + phone);
        // 2.对比验证码
        if (!StrUtil.equals(verificationCode, codeFromRedis)) {
            return ResponseEntity.status(500).body(ErrorResult.loginError());
        }
        // 3.根据手机号查询
        User user = userService.findByPhone(phone);
        Boolean isNew;
        if (user != null) {
            // 3-1 老用户
            isNew = false;
        } else {
            // 3-2 新用户
            isNew = true;
            // 保存新用户
            user = new User();
            user.setMobile(phone);
            user.setPassword("123456");
            Long userId = userService.save(user);
            user.setId(userId);
        }

        // 4.制作jwt令牌
        user.setPassword(null);
        Map<String, Object> claims = BeanUtil.beanToMap(user);
        String token = JwtUtil.createToken(claims);

        // 5.向redis中存储用户信息（续期）
        String json = JSON.toJSONString(user);
        stringRedisTemplate.opsForValue().set(ConstantUtil.USER_TOKEN+token, json, Duration.ofDays(7));


        // 清除redis中验证码
        stringRedisTemplate.delete(ConstantUtil.SMS_CODE + phone);

        // 6.返回结果
        HashMap<String,Object> resultMap = new HashMap<>();
        resultMap.put("isNew", isNew);
        resultMap.put("token", token);

        return ResponseEntity.ok(resultMap);
    }
}
