package com.itheima.app.controller;

import com.itheima.domain.db.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserManager userManager;

    //1.保存用户
    @PostMapping("/user/save")
    public ResponseEntity save(@RequestBody User user){
        return userManager.save(user);
    }

    //2.根据手机号查询
    @GetMapping("/user/findByPhone")
    public ResponseEntity findByPhone(String phone){
        return userManager.findByPhone(phone);
    }

    // 发送短信验证码
    @PostMapping("/user/login")
    public void sendSms(@RequestBody Map<String,String> param){
        // 1.接收请求参数
        String phone = param.get("phone");
        // 2.调用manager
        userManager.sendSms(phone);
    }

    // 注册登录
    @PostMapping("/user/loginVerification")
    public ResponseEntity regAndLogin(@RequestBody Map<String, String> param) {
        // 1.接收请求参数
        String phone = param.get("phone");
        String verificationCode = param.get("verificationCode");
        // 2.调用manager
        return userManager.regAndLogin(phone, verificationCode);
    }
}
